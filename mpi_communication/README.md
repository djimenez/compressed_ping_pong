# Compressed MPI Communications First Tests

This directory holds several initial benchmarks developed to test simple MPI communications with compressed data. 

The data is compressed with the ZFP library by creating compressor objects declared inside both compressor.h and compressor_gpu.h 
    - A compressor object in this header file has 5 different components:
    1. A pointer of type zfp_field: this is meta data returned by the ZFP library when calling the different zfp_field_xd functions. 
    2. The data type that is going to be operated on (either zfp_tpe_float or zfp_type_double)
    3. A double named user_mode_value: this basically holds the user defined values for precision, accuracy or rate compression. 
    4. An unsigned char pointer to the compressed data. When calling the compress methods inside the compressor, this pointer will be returned. 
    5. An int backend_flag: this determines whether the compression/decompression will occur on CPU or GPU. 


## Benchmarks:

- cpp_gpu_single_size.cpp: after some testing of the first three versions, we realized that there was some strange timing results on fixed-rate compression that made communication speedups excessive. We got the suggestion
                            to make the executable work on only one problem size at a time. This version implements a single-size ping-pong benchmark. The message size is user defined and passed as command line parameter. 
- breakdown_cpp_gpu.cpp: This version implements a manual device-to-device transfer where a cudaMemcpy then MPI send/receive and then cudaMemcpy are used to measure separete bandwidths along the communications. 
- breakdown_cpp_gpu_per_rank.cpp: Same as the previous breakdown but timings are reported by MPI rank and not only a single value of time. This can be used to analyze possible imbalance issues. 
- multi_pair_overlapped_asynchronous_gpu.cpp: an optimization is used to overlap data transfers from GPU-to-CPU and MPI communications. This version uses cudaStreams, cudaMemcpyAsync, MPI_Isend/MPI_Irecv to execute the ping-pong. 

