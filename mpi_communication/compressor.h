//File :: compressor.h
//Created :: Tue Mai 30, 2023
//Description :: Floating-point compression/decompression class that uses ZFP fixed accuracy mode
//Based on 2018 ETH Zurich exercise 10 on: https://www.cse-lab.ethz.ch/teaching/hpcse-i_hs18/

#include <cassert>
#include <iostream>
#include <cstdlib>

#include "zfp.h"

using namespace std;

template <typename DataType>
class ZFPCompressor{
    private:
        zfp_field* meta_data;
        zfp_type data_type;
        double user_accuracy;
        unsigned char* compressed_buffer;
    public:
        // Constructor for 1D ZFP Field
        ZFPCompressor(DataType* const data_to_compress, const size_t dim_1, const double tolerance) :
            user_accuracy(tolerance), compressed_buffer(nullptr)
        {
                data_type = (sizeof(DataType)==4) ? zfp_type_float : zfp_type_double;
                meta_data = zfp_field_1d(data_to_compress, data_type, dim_1);
                //cout << "Creating compressor 1D object of type: " << data_type << endl;
        }
        
        // Constructor for 2D ZFP Field
        ZFPCompressor(DataType* const data_to_compress, const size_t dim_1, const size_t dim_2, const double tolerance) :
            user_accuracy(tolerance), compressed_buffer(nullptr)
        {
                data_type = (sizeof(DataType)==4) ? zfp_type_float : zfp_type_double;
                meta_data = zfp_field_2d(data_to_compress, data_type, dim_1, dim_2);
                //cout << "Creating compressor 2D object of type: " << data_type << endl;
        }
        
        // Constructor for 3D ZFP Field
        ZFPCompressor(DataType* const data_to_compress, const size_t dim_1, const size_t dim_2, const size_t dim_3, const double tolerance) :
            user_accuracy(tolerance), compressed_buffer(nullptr)
        {
                data_type = (sizeof(DataType)==4) ? zfp_type_float : zfp_type_double;
                meta_data = zfp_field_3d(data_to_compress, data_type, dim_1, dim_2, dim_3);
                //cout << "Creating compressor 3D object of type: " << data_type << endl;
        }
        
        // Constructor for 4D ZFP Field
        ZFPCompressor(DataType* const data_to_compress, const size_t dim_1, const size_t dim_2, const size_t dim_3, const size_t dim_4, const double tolerance) :
            user_accuracy(tolerance), compressed_buffer(nullptr)
        {
                data_type = (sizeof(DataType)==4) ? zfp_type_float : zfp_type_double;
                meta_data = zfp_field_4d(data_to_compress, data_type, dim_1, dim_2, dim_3, dim_4);
                //cout << "Creating compressor 4D object of type: " << data_type << endl;

        }
        
        ~ZFPCompressor()
        {
            if (compressed_buffer){
                delete [] compressed_buffer;
                compressed_buffer = nullptr;
            }
            zfp_field_free(meta_data);
        }

        void ZFPCompressorSetStrides(ptrdiff_t x_s){
            zfp_field_set_stride_1d(meta_data, x_s);
        }

        void ZFPCompressorSetStrides(ptrdiff_t x_s, ptrdiff_t y_s){
            zfp_field_set_stride_2d(meta_data, x_s, y_s);
        }
        
        void ZFPCompressorSetStrides(ptrdiff_t x_s, ptrdiff_t y_s, ptrdiff_t z_s){
            zfp_field_set_stride_3d(meta_data, x_s, y_s, z_s);
        }

        void ZFPCompressorSetStrides(ptrdiff_t x_s, ptrdiff_t y_s, ptrdiff_t z_s, ptrdiff_t w_s){
            zfp_field_set_stride_4d(meta_data, x_s, y_s, z_s, w_s);
        }

        unsigned char* compress(size_t& buffer_size, size_t& compressed_bytes, int num_threads){
            zfp_stream* zfp = zfp_stream_open(nullptr);
            zfp_stream_set_accuracy(zfp, user_accuracy);
            
            buffer_size = zfp_stream_maximum_size(zfp, meta_data);
            assert(buffer_size > 0);
            if (compressed_buffer) free(compressed_buffer); //clear previous compressed buffer
            compressed_buffer = new unsigned char[buffer_size];

            bitstream* stream = stream_open(compressed_buffer, buffer_size);
            zfp_stream_set_bit_stream(zfp, stream);
            //zfp_stream_set_execution(zfp, zfp_exec_omp);
            zfp_stream_rewind(zfp);
            
            //zfp_compress returns the resulting byte offset within the bit stream
            //this equals the number of bytes of compressed storage IF the stream
            //was rewound before the call to compress.
            if(zfp_stream_set_execution(zfp,zfp_exec_omp)){
                zfp_stream_set_omp_threads(zfp, num_threads);
                compressed_bytes = zfp_compress(zfp, meta_data);
                assert(compressed_bytes > 0);
            }
            zfp_stream_close(zfp);
            stream_close(stream);
            return compressed_buffer;
        }

        DataType* decompress(unsigned char* compressed_data, const size_t buffer_size){
            zfp_stream* zfp = zfp_stream_open(nullptr);
            zfp_stream_set_accuracy(zfp, user_accuracy);

            const size_t local_buffersize = zfp_stream_maximum_size(zfp, meta_data);
            assert(buffer_size == local_buffersize);

            bitstream* stream = stream_open(compressed_data, buffer_size);
            zfp_stream_set_bit_stream(zfp, stream);
            zfp_stream_rewind(zfp);

            if(zfp_decompress(zfp, meta_data) == 0){
                std::cerr << "Decompression failed" << std::endl;
            }

            zfp_stream_close(zfp);
            stream_close(stream);
            return static_cast<DataType*>(meta_data->data);
        }

};

