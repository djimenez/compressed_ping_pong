/*
    This code implements a ping-pong benchmark among multiple pairs of  
    of MPI ranks. However, in this particular version it is possible to use fixed-rate compression
    on both CPU and GPU. Not only fixed-accuracy compression. 
    For an even number of ranks, the first N/2-th ranks initialize the data and
    communicate with the second half of the ranks. 
    First: data is initialized on first N/2-th ranks.
    Second: an uncompressed version of the ping-pong is executed to use as 
    performance baseline.
    Third: data is compressed on all first N/2-th ranks. 
    Fourth: a compressed ping-pong is executed to compare performance 
    of compressed communications.

    If running with GPU as backends for compression/decompression, the MPI_Send and Recv operations
    operate over the device pointers, relying on CUDA-aware MPI.
*/

#include <cstdio>
#include <stdlib.h>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <cuda_runtime.h>
#include <chrono>
#include "compressor_gpu.h"
#include "mpi.h"


#define MAX 200

void writeArrayToFile(const double* array, int size, int number, int rank) {
    std::string output = "output_";
	std::stringstream filename;
    filename << output << size <<"_rank_"<<rank<<"_" <<number << ".txt";

    std::ofstream file(filename.str());
    if (!file) {
        std::cerr << "Failed to open the file." << std::endl;
        return;
    }

    for (int i = 0; i < size; ++i) {
        file << array[i] << "\n";
    }

    file.close();
    //std::cout << "Array successfully written to " << filename.str() << std::endl;
}

void writeArrayToFile(const double* array, int size, int number) {
    std::string output = "output_";
	std::stringstream filename;
    filename << output << size <<"_"<< number << ".txt";

    std::ofstream file(filename.str());
    if (!file) {
        std::cerr << "Failed to open the file." << std::endl;
        return;
    }

    for (int i = 0; i < size; ++i) {
        file << array[i] << "\n";
    }

    file.close();
    //std::cout << "Array successfully written to " << filename.str() << std::endl;
}

long double compute_l2_error(const double* original_data, const double* compressed_data, int size){
    long double l2_error = 0.0;
    long double loc_error = 0.0;
    for(int i=0;i<size;i++){
        loc_error = compressed_data[i]-original_data[i];
        l2_error += loc_error*loc_error;
    }
    l2_error = sqrt(l2_error/size);
    std::cout << "L2 error is: " << l2_error << std::endl;
    return l2_error;
}

void writeResultsToCSV(const int* array_sizes,const double* uncompressed_times, const double* compressed_times, const double* compression_times, const double* decompression_times,
                        const double* zfp_overhead, const double* compression_ratios, const double* communication_speedups, const double* actual_performance_gains, const int numThreads, const double accuracy,
                        const long double* rmse, const double* transferDtoH_times, const double* transferHtoD_times){
    int my_rank, comm_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    
    std::string output = "results";
    std::stringstream filename;
    filename << output << "_" << accuracy <<"_" << numThreads << ".csv";

    std::ofstream file(filename.str(), std::ios::app);
    if (!file) {
        std::cerr << "Failed to open the file." << std::endl;
        return;
    }

    if(file.tellp()==0){
        file << "Array Size [B]" <<",Rank"<< ",Unc. Time (s)" <<",Com. Time (s)"<<",Comp. Ratio" << ",Comm. Speedup" <<",Compression Time"<<",Decompression Time"<<",ZFP Overhead"<<",Actual Perf. Gain"<<",RMSE"<<",DtoH_time"<<"HtoD_time\n";
    }
    for(int i=0; i<comm_size/2; i++){
        file << array_sizes[i] <<"," << i << ","<<uncompressed_times[i] << "," << compressed_times[i] << ","<<compression_ratios[i]<<","<< communication_speedups[i] <<
            "," <<compression_times[i]<<","<<decompression_times[i]<<","<<zfp_overhead[i]<<","<< actual_performance_gains[i] <<","<< rmse[i] << "," << transferDtoH_times[i] <<","<< transferHtoD_times[i]<<"\n";
    }
    file.close();

}
void writeResultsToCSV(const int* array_sizes,const double* uncompressed_times, const double* compressed_times, const double* compression_times, const double* decompression_times,
                        const double* zfp_overhead, const double* compression_ratios, const double* communication_speedups, const double* actual_performance_gains, const int numThreads, const double accuracy,
                        const long double* rmse){
    int my_rank, comm_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    std::string output = "results";
    std::stringstream filename;
    filename << output << "_" << accuracy <<"_" << numThreads << ".csv";

    std::ofstream file(filename.str(), std::ios::app);
    if (!file) {
        std::cerr << "Failed to open the file." << std::endl;
        return;
    }

    if(file.tellp()==0){
        file << "Array Size [B]" <<",Rank"<< ",Unc. Time (s)" <<",Com. Time (s)"<<",Comp. Ratio" << ",Comm. Speedup" <<",Compression Time"<<",Decompression Time"<<",ZFP Overhead"<<",Actual Perf. Gain"<<",RMSE\n";
    }
    for(int i=0; i<comm_size/2; i++){
        file << array_sizes[i] <<"," << i << ","<<uncompressed_times[i] << "," << compressed_times[i] << ","<<compression_ratios[i]<<","<< communication_speedups[i] <<
            "," <<compression_times[i]<<","<<decompression_times[i]<<","<<zfp_overhead[i]<<","<< actual_performance_gains[i] <<","<< rmse[i] << "\n";
    }
    file.close();

}

void initialize_array_on_host(double* host_array, int size){
    for (int i= 0; i < size; i++) {
        double smoothValue = 50*std::sin(2.0 * M_PI * i / size);
        host_array[i] = smoothValue;
    }
}

void execute_uncompressed_ping_pong(double* data, int m_size, int* array_sizes, double* uncompressed_times, int backend_flag, double* transferDtoH_times, double* transferHtoD_times){
    int my_rank, comm_size, num_devices, deviceID, numberOfSMs;
    double htod_start_time, htod_end_time, htod_time;
    double dtoh_start_time, dtoh_end_time, dtoh_time;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    int couple = (my_rank+comm_size/2)%comm_size;
    int color = (my_rank < comm_size / 2) ? 0 : MPI_UNDEFINED;
    MPI_Comm first_half_ranks;
    MPI_Comm_split(MPI_COMM_WORLD, color, my_rank, &first_half_ranks);
    MPI_Status status;
    int k = 0;
    double uncompressed_time;
    auto total_duration = std::chrono::microseconds(0);
    double* original_data_from_device;
    double* final_data_from_device; 
    
    if(backend_flag == 1){
        //Verify that each rank gets one GPU    
        cudaGetDeviceCount(&num_devices);
        cudaSetDevice(my_rank%num_devices);
        cudaGetDevice(&deviceID);
        cudaDeviceGetAttribute(&numberOfSMs, cudaDevAttrMultiProcessorCount, deviceID);

        const int num_streams = 4;
        const int stream_size = m_size/num_streams;
        const int stream_bytes = stream_size*sizeof(double);

        cudaStream_t stream[num_streams];
        for(int i=0;i<num_streams;i++){cudaStreamCreate(&stream[i]);}
        
        double* data_host;
        cudaMallocHost((void**)&data_host, m_size*sizeof(double));
        MPI_Request send_requests[num_streams];
        MPI_Request recv_requests[num_streams];
        MPI_Status stats[8];

        
        if(my_rank < comm_size/2){

            //original_data_from_device = new double[m_size];
            //cudaMemcpy(original_data_from_device,data, m_size*sizeof(double),cudaMemcpyDeviceToHost); 
            //cudaError_t err = cudaGetLastError();
            //if (err != cudaSuccess) {
            //    printf("CUDA error: %s\n", cudaGetErrorString(err));
            //    exit(-1);
            //}
            //writeArrayToFile(original_data_from_device, m_size, 1, my_rank);
            //delete[] original_data_from_device;
            int array_size = m_size*sizeof(double);
            MPI_Gather(&array_size, 1, MPI_INT, array_sizes, 1, MPI_INT, 0, first_half_ranks);
            for(int pass = 0; pass < MAX; pass++){
                MPI_Barrier(MPI_COMM_WORLD);
                auto upp_start_time = std::chrono::high_resolution_clock::now();
                for(int i=0;i<num_streams;i++){
                    int offset = i*stream_size;
                    cudaMemcpyAsync(&data_host[offset],&data[offset],stream_bytes,cudaMemcpyDeviceToHost,stream[i]);
                }
                for(int i=0;i<num_streams;i++){
                    cudaStreamSynchronize(stream[i]);
                }

                for(int i=0;i<num_streams;i++){
                    int offset = i*stream_size;
                    MPI_Isend(&data_host[offset], stream_size, MPI_DOUBLE, couple, i, MPI_COMM_WORLD,&send_requests[i]);
                    MPI_Irecv(&data_host[offset], stream_size, MPI_DOUBLE, couple, i, MPI_COMM_WORLD,&recv_requests[i]);
                }
                for(int i=0;i<num_streams;i++){
                    int offset = i*stream_size;
                    MPI_Wait(&recv_requests[i],&stats[i+num_streams]);
                    cudaMemcpyAsync(&data[offset],&data_host[offset],stream_bytes,cudaMemcpyHostToDevice,stream[i]);
                }
                for(int i=0;i<num_streams;i++){
                    cudaStreamSynchronize(stream[i]);
                    MPI_Wait(&send_requests[i],&stats[i+num_streams]);
                }
                auto upp_end_time = std::chrono::high_resolution_clock::now();
                if(pass>MAX/2){
                    auto iteration_duration = std::chrono::duration_cast<std::chrono::microseconds>(upp_end_time - upp_start_time);
                    total_duration += iteration_duration;
                }
           }
           if(my_rank == 0){std::cout << "Ping-pong completed, starting data reduction" << std::endl; }
           double total_duration_double = static_cast<double>(total_duration.count());
           uncompressed_time = (total_duration_double/(MAX/2))/1000000;
           std::cout << "Rank " << my_rank << " uncompressed time: " << uncompressed_time <<std::endl;
           MPI_Gather(&uncompressed_time, 1, MPI_DOUBLE, uncompressed_times, 1, MPI_DOUBLE, 0, first_half_ranks);
           if(my_rank == 0){std::cout << "Gathered uncompressed times" << std::endl; }
        
           //final_data_from_device = new double[m_size];
           //cudaMemcpy(final_data_from_device,data, m_size*sizeof(double),cudaMemcpyDeviceToHost); 
           //cudaError_t err_2 = cudaGetLastError();
           //if (err_2 != cudaSuccess) {
           //    printf("CUDA error: %s\n", cudaGetErrorString(err));
           //    exit(-1);
           //}
           //writeArrayToFile(final_data_from_device, m_size, 2, my_rank);
           //delete[] final_data_from_device;
        }
        else{
            for(int pass = 0; pass < MAX; pass++){
                MPI_Barrier(MPI_COMM_WORLD);
                for(int i=0;i<num_streams;i++){
                    int offset = i*stream_size;
                    MPI_Irecv(&data_host[offset], stream_size, MPI_DOUBLE, couple, i, MPI_COMM_WORLD, &recv_requests[i]);
                    MPI_Wait(&recv_requests[i],&stats[i+num_streams]);
                    MPI_Isend(&data_host[offset], stream_size, MPI_DOUBLE, couple, i, MPI_COMM_WORLD,&send_requests[i]);
                }
                for(int i=0;i<num_streams;i++){
                    MPI_Wait(&send_requests[i],&stats[i]);
                }
            }
        }
        cudaFreeHost(data_host);
        std::cout << "Rank " << my_rank << " exit uncompressed ping-pong " <<std::endl;
    }
}


void execute_uncompressed_ping_pong(double* data, int m_size, int* array_sizes, double* uncompressed_times, int backend_flag){
    int my_rank, comm_size, num_devices, deviceID, numberOfSMs;
    double upp_start_time, upp_end_time, upp_time, uncompressed_time; //Timing for uncompressed ping-pong
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    int couple = (my_rank+comm_size/2)%comm_size;
    int color = (my_rank < comm_size / 2) ? 0 : MPI_UNDEFINED;
    MPI_Comm first_half_ranks;
    MPI_Comm_split(MPI_COMM_WORLD, color, my_rank, &first_half_ranks);
    MPI_Status status;
    int k = 0;
    double* original_data_from_device, final_data_from_device;
    
    if(backend_flag == 1){
        //Verify that each rank gets one GPU    
        cudaGetDeviceCount(&num_devices);
        cudaSetDevice(my_rank%num_devices);
        cudaGetDevice(&deviceID);
        cudaDeviceGetAttribute(&numberOfSMs, cudaDevAttrMultiProcessorCount, deviceID);
        //std::cout << "Rank "<< my_rank <<"Device ID: " << deviceID << ", Number of SMs: " << numberOfSMs << std::endl;

        double* data_host;
        cudaMallocHost((void**)&data_host, m_size*sizeof(double));


        if(my_rank < comm_size/2){
            if(my_rank == 0){
                array_sizes[k] = m_size*sizeof(double);
            }
            for(int pass = 0; pass < MAX; pass++){
                MPI_Barrier(MPI_COMM_WORLD);
                if(my_rank==0 && pass>MAX/2){
                    upp_start_time = MPI_Wtime();
                }
                cudaMemcpy(data_host,data, m_size*sizeof(double), cudaMemcpyDeviceToHost);
                MPI_Send(data_host, m_size, MPI_DOUBLE, couple, 0, MPI_COMM_WORLD);
                MPI_Recv(data_host, m_size, MPI_DOUBLE, couple, 0, MPI_COMM_WORLD, &status);
                cudaMemcpy(data,data_host, m_size*sizeof(double), cudaMemcpyHostToDevice);
                MPI_Barrier(first_half_ranks);
                if(my_rank==0 && pass>MAX/2){
                    upp_end_time = MPI_Wtime();
                    upp_time+=upp_end_time-upp_start_time;
                }
            }
            if(my_rank == 0){
                uncompressed_time = upp_time/(MAX/2);
                uncompressed_times[k] = uncompressed_time;
                std::cout << "m_size "<< m_size <<" Uncompressed time: "<< uncompressed_time <<std::endl;
            }
        }
        else{
            for(int pass = 0; pass < MAX; pass++){
                MPI_Barrier(MPI_COMM_WORLD);
                MPI_Recv(data_host, m_size, MPI_DOUBLE, couple, 0, MPI_COMM_WORLD, &status);
                MPI_Send(data_host, m_size, MPI_DOUBLE, couple, 0, MPI_COMM_WORLD);
            }
        }
    }else{

        if(my_rank < comm_size/2){
            if(my_rank == 0){
                array_sizes[k] = m_size*sizeof(double);
            }
            for(int pass = 0; pass < MAX; pass++){
                MPI_Barrier(MPI_COMM_WORLD);
                if(my_rank==0 && pass>MAX/2){
                    upp_start_time = MPI_Wtime();
                }
                MPI_Send(data, m_size, MPI_DOUBLE, couple, 0, MPI_COMM_WORLD);
                MPI_Recv(data, m_size, MPI_DOUBLE, couple, 0, MPI_COMM_WORLD, &status);
                MPI_Barrier(first_half_ranks);
                if(my_rank==0 && pass>MAX/2){
                    upp_end_time = MPI_Wtime();
                    upp_time+=upp_end_time-upp_start_time;
                }
            }
            if(my_rank == 0){
                uncompressed_time = upp_time/(MAX/2);
                uncompressed_times[k] = uncompressed_time;
                std::cout << "m_size "<< m_size <<" Uncompressed time: "<< uncompressed_time <<std::endl;
            }
        }
        else{
            for(int pass = 0; pass < MAX; pass++){
                MPI_Barrier(MPI_COMM_WORLD);
                MPI_Recv(data, m_size, MPI_DOUBLE, couple, 0, MPI_COMM_WORLD, &status);
                MPI_Send(data, m_size, MPI_DOUBLE, couple, 0, MPI_COMM_WORLD);
            }
        }
    }
}

void execute_compressed_ping_pong(unsigned char* compressed_data, const int compressed_bytes, double* compressed_times, const int m_size, const int backend_flag){
    int my_rank, comm_size;
    int num_devices, deviceID, numberOfSMs;
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    int couple = (my_rank+comm_size/2)%comm_size;
    int color = (my_rank < comm_size / 2) ? 0 : MPI_UNDEFINED;
    MPI_Comm first_half_ranks;
    MPI_Comm_split(MPI_COMM_WORLD, color, my_rank, &first_half_ranks);
    MPI_Status status;
    //double cpp_start_time, cpp_end_time, cpp_time, compressed_time; //Timing for compressed ping-pong
    double compressed_time;
    auto total_duration = std::chrono::microseconds(0);


    
    if(backend_flag == 1){
        cudaGetDeviceCount(&num_devices);
        cudaSetDevice(my_rank%num_devices);
        cudaGetDevice(&deviceID);
        cudaDeviceGetAttribute(&numberOfSMs, cudaDevAttrMultiProcessorCount, deviceID);
        
        const int num_streams = 4;
        const int stream_size = compressed_bytes/num_streams;

        cudaStream_t stream[num_streams];
        for(int i=0;i<num_streams;i++){cudaStreamCreate(&stream[i]);}
    
        unsigned char* data_host;
        cudaMallocHost((void**)&data_host, compressed_bytes);
        MPI_Request send_requests[num_streams];
        MPI_Request recv_requests[num_streams];
        MPI_Status stats[8];


        if(my_rank < comm_size/2){

            //unsigned char* data_host_probe;
            //cudaMallocHost((void**)&data_host_probe, compressed_bytes);
            //cudaMemcpy(data_host_probe,compressed_data,compressed_bytes,cudaMemcpyDeviceToHost);
            //std::string output = "before_rank_";
	        //std::stringstream filename;
            //filename << output << my_rank << ".bin";
            //std::ofstream file(filename.str(),std::ios::binary);
            //if (file.is_open()) {
            //    file.write(reinterpret_cast<const char*>(data_host_probe), sizeof(data_host_probe));
            //    file.close();
            //} else {
            //    std::cerr << "Unable to open the output file." << std::endl;
            //}
            for(int pass = 0; pass < MAX; pass++){
                MPI_Barrier(MPI_COMM_WORLD);
                
                auto cpp_start_time = std::chrono::high_resolution_clock::now();
                for(int i=0;i<num_streams;i++){
                    int offset = i*stream_size;
                    cudaMemcpyAsync(&data_host[offset],&compressed_data[offset],stream_size,cudaMemcpyDeviceToHost,stream[i]);
                }
                for(int i=0;i<num_streams;i++){
                    cudaStreamSynchronize(stream[i]);
                }
                for(int i=0;i<num_streams;i++){
                    int offset = i*stream_size;
                    MPI_Isend(&data_host[offset], stream_size, MPI_UNSIGNED_CHAR, couple, i, MPI_COMM_WORLD,&send_requests[i]);
                    MPI_Irecv(&data_host[offset], stream_size, MPI_UNSIGNED_CHAR, couple, i, MPI_COMM_WORLD,&recv_requests[i]);
                }
                for(int i=0;i<num_streams;i++){
                    int offset = i*stream_size;
                    MPI_Wait(&recv_requests[i],&stats[i+num_streams]);
                    cudaMemcpyAsync(&compressed_data[offset],&data_host[offset],stream_size,cudaMemcpyHostToDevice,stream[i]);
                }
                for(int i=0;i<num_streams;i++){
                    cudaStreamSynchronize(stream[i]);
                    MPI_Wait(&send_requests[i],&stats[i]);
                }
                auto cpp_end_time = std::chrono::high_resolution_clock::now();
                if(pass>MAX/2){
                    auto iteration_duration = std::chrono::duration_cast<std::chrono::microseconds>(cpp_end_time - cpp_start_time);
                    total_duration += iteration_duration;
                }
           }
           if(my_rank == 0){std::cout << "Compressed Ping-pong completed, starting data reduction" << std::endl; }
           double total_duration_double = static_cast<double>(total_duration.count());
           compressed_time = (total_duration_double/(MAX/2))/1000000;
           std::cout << "Rank " << my_rank << " compressed time: " << compressed_time <<std::endl;
           MPI_Gather(&compressed_time, 1, MPI_DOUBLE, compressed_times, 1, MPI_DOUBLE, 0, first_half_ranks);
           if(my_rank == 0){std::cout << "Gathered compressed times" << std::endl; }
           //std::string output_2 = "after_rank_";
	       //std::stringstream filename_2;
           //filename_2 << output_2 << my_rank << ".bin";
           //std::ofstream file_2(filename_2.str(),std::ios::binary);

           //if (file_2.is_open()) {
           // file_2.write(reinterpret_cast<const char*>(data_host), sizeof(data_host));
           // file_2.close();
           //} else {
           //     std::cerr << "Unable to open the output file." << std::endl;
           // }
        }else{
            for(int pass = 0; pass < MAX; pass++){
                MPI_Barrier(MPI_COMM_WORLD);
                for(int i=0;i<num_streams;i++){
                    int offset = i*stream_size;
                    MPI_Irecv(&data_host[offset], stream_size, MPI_UNSIGNED_CHAR, couple, i, MPI_COMM_WORLD, &recv_requests[i]);
                    MPI_Wait(&recv_requests[i],&stats[i+num_streams]);
                    MPI_Isend(&data_host[offset], stream_size, MPI_UNSIGNED_CHAR, couple, i, MPI_COMM_WORLD,&send_requests[i]);
                }
                for(int i=0;i<num_streams;i++){
                    MPI_Wait(&send_requests[i],&stats[i]);
                }
            }
        }
        cudaFreeHost(data_host);
    }
}

void prepare_compressed_ping_pong(double* data, int m_size, int mode, int backend_flag, double user_value, 
                                    int numThreads, double* compressed_times, double* compression_times, double* decompression_times,
                                    double* zfp_overhead, double* compression_ratios, long double* rmse){
    int my_rank, comm_size;
    unsigned char* compressed_data;
    double* decompressed_data_host;
    double* decompressed_data_device;
    double* original_data_from_device;
    size_t bufsize;
    size_t compressed_bytes;
     //Timers for different stages
    double comp_start_time, comp_end_time, comp_time; //Timing for compression
    double dec_start_time, dec_end_time, dec_time; //Timing for decompression
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    int couple = (my_rank+comm_size/2)%comm_size;
    int color = (my_rank < comm_size / 2) ? 0 : MPI_UNDEFINED;
    MPI_Comm first_half_ranks;
    MPI_Comm_split(MPI_COMM_WORLD, color, my_rank, &first_half_ranks);
    MPI_Status status;
    cudaError_t mallocErr, kernelErr, syncErr;
    int num_devices, deviceID, numberOfSMs;
    int i = 0; 

    if(my_rank < comm_size/2){ //First half of ranks compress the data, send, receive and decompress data
        if(mode == 0){ //Mode 0 = use fixed-rate compression
            if(backend_flag == 0){ //backend_flag = 0 : execute on CPU
                if(my_rank == 0){
                    //writeArrayToFile(data, m_size, 0);
                }
                //All first half ranks initialize their own compressor
                ZFPCompressor<double> compressor(data, m_size, user_value, backend_flag);
                MPI_Barrier(first_half_ranks); 
                comp_start_time = MPI_Wtime();
                compressed_data = compressor.compress_fixed_rate(bufsize, compressed_bytes, numThreads);
                
                comp_end_time = MPI_Wtime();
                comp_time = comp_end_time - comp_start_time;
                MPI_Gather(&comp_time, 1, MPI_DOUBLE, compression_times, 1, MPI_DOUBLE, 0, first_half_ranks);
                double comp_ratio =  static_cast<double>(m_size)*sizeof(double)/compressed_bytes;
                MPI_Gather(&comp_ratio, 1, MPI_DOUBLE, compression_ratios, 1, MPI_DOUBLE, 0, first_half_ranks);
                    
                //if(my_rank==0){
                    //This decompression is just for testing and can be deleted
                    //decompressed_data_host = new double[m_size]; 
                    //ZFPCompressor<double> decompressor(decompressed_data_host, m_size, user_value, backend_flag);
                    //decompressor.decompress_fixed_rate(compressed_data, bufsize);
                    //writeArrayToFile(decompressed_data_host, m_size, 1);
                    //delete[] decompressed_data_host;
                //}
                //Send information on amount of bytes to transfer
                MPI_Send(&compressed_bytes, 1, MPI_UNSIGNED_LONG_LONG, couple, 3, MPI_COMM_WORLD);
                
                if(my_rank==0){std::cout << "Executing compressed ping-pong" << std::endl;}
                //Execute compressed ping pong here
                execute_compressed_ping_pong(compressed_data, compressed_bytes, compressed_times, m_size,backend_flag);
    
                decompressed_data_host = new double[m_size]; 
                ZFPCompressor<double> decompressor(decompressed_data_host, m_size, user_value, backend_flag);
                
                dec_start_time = MPI_Wtime();
                decompressor.decompress_fixed_rate(compressed_data, bufsize);
                dec_end_time = MPI_Wtime();
                dec_time = dec_end_time - dec_start_time;
                MPI_Gather(&dec_time, 1, MPI_DOUBLE, decompression_times, 1, MPI_DOUBLE, 0, first_half_ranks);
                double overhead = compression_times[i]+decompression_times[i];
                MPI_Gather(&overhead, 1, MPI_DOUBLE, zfp_overhead, 1, MPI_DOUBLE, 0, first_half_ranks);
                //writeArrayToFile(decompressed_data_host, m_size, 2);
                long double root_square_error = compute_l2_error(data, decompressed_data_host, m_size);
                MPI_Gather(&root_square_error, 1, MPI_LONG_DOUBLE, rmse, 1, MPI_LONG_DOUBLE, 0, first_half_ranks);
                delete[] decompressed_data_host;
            }
            else if(backend_flag == 1){ //backend_flag = 1: execute on GPU
                cudaGetDeviceCount(&num_devices);
                cudaSetDevice(my_rank%num_devices);
                cudaGetDevice(&deviceID);
                cudaDeviceGetAttribute(&numberOfSMs, cudaDevAttrMultiProcessorCount, deviceID);
                
                // Copy original data from device to host
                // This will be used at the end of step to compute RMSE
                if(my_rank == 0){std::cout << "Inside prepare_compressed_ping-pong" << std::endl;}
                original_data_from_device = new double[m_size];
                cudaMemcpy(original_data_from_device,data, m_size*sizeof(double),cudaMemcpyDeviceToHost); 
                cudaError_t err = cudaGetLastError();
                if (err != cudaSuccess) {
                    printf("CUDA error: %s\n", cudaGetErrorString(err));
                    exit(-1);
                }
                //writeArrayToFile(original_data_from_device, m_size, 1, my_rank);
    
    
                ZFPCompressor<double> compressor(data, m_size, user_value, backend_flag);
                
                comp_start_time = MPI_Wtime();
                compressed_data = compressor.compress_fixed_rate(bufsize, compressed_bytes,numThreads);
                comp_end_time = MPI_Wtime();
                comp_time = comp_end_time - comp_start_time;
                MPI_Gather(&comp_time, 1, MPI_DOUBLE, compression_times, 1, MPI_DOUBLE, 0, first_half_ranks);
                double comp_ratio =  static_cast<double>(m_size)*sizeof(double)/compressed_bytes;
                MPI_Gather(&comp_ratio, 1, MPI_DOUBLE, compression_ratios, 1, MPI_DOUBLE, 0, first_half_ranks);
                
                //Send information on amount of bytes to transfer and the buffer size
                MPI_Send(&compressed_bytes, 1, MPI_UNSIGNED_LONG_LONG, couple, 3, MPI_COMM_WORLD);
    
                //Execute compressed ping pong here
                if(my_rank==0){std::cout << "Executing compressed ping-pong" << std::endl;}
                execute_compressed_ping_pong(compressed_data, compressed_bytes, compressed_times, m_size,backend_flag);
                if(my_rank==0){std::cout << "Compressed ping-pong done" << std::endl;}
                
                mallocErr = cudaMalloc(&decompressed_data_device, m_size*sizeof(double));
                if(mallocErr != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(mallocErr));
                ZFPCompressor<double> decompressor(decompressed_data_device, m_size, user_value, backend_flag);
                dec_start_time = MPI_Wtime();
                decompressor.decompress_fixed_rate(compressed_data, bufsize);
                
                if(my_rank==0){std::cout << "Decompress complete" << std::endl;}
                dec_end_time = MPI_Wtime();
                dec_time = dec_end_time - dec_start_time;
                MPI_Gather(&dec_time, 1, MPI_DOUBLE, decompression_times, 1, MPI_DOUBLE, 0, first_half_ranks);
                double overhead = comp_time + dec_time;
                MPI_Gather(&overhead, 1, MPI_DOUBLE, zfp_overhead, 1, MPI_DOUBLE, 0, first_half_ranks);
                if(my_rank==0){std::cout << "Decompress timings complete" << std::endl;}
                
                decompressed_data_host = new double[m_size];
                cudaMemcpy(decompressed_data_host,decompressed_data_device, m_size*sizeof(double),cudaMemcpyDeviceToHost); 
                //writeArrayToFile(decompressed_data_host, m_size, 3,my_rank);
                long double root_square_error = compute_l2_error(decompressed_data_host, original_data_from_device, m_size);
                MPI_Gather(&root_square_error, 1, MPI_LONG_DOUBLE, rmse, 1, MPI_LONG_DOUBLE, 0, first_half_ranks);
                delete[] decompressed_data_host;
                delete[] original_data_from_device;
                
                cudaFree(decompressed_data_device);
            }
        }
        else if(mode == 1){ //mode == 1 : execute fixed-accuracy compression
            if(my_rank ==0){
                //writeArrayToFile(data, m_size, 0);
            }
            ZFPCompressor<double> compressor(data, m_size, user_value, backend_flag); //Always on CPU
            comp_start_time = MPI_Wtime();
            compressed_data = compressor.compress_fixed_accuracy(bufsize, compressed_bytes,numThreads);
            
            comp_end_time = MPI_Wtime();
            comp_time = comp_end_time - comp_start_time;
            MPI_Gather(&comp_time, 1, MPI_DOUBLE, compression_times, 1, MPI_DOUBLE, 0, first_half_ranks);
            double comp_ratio =  static_cast<double>(m_size)*sizeof(double)/compressed_bytes;
            MPI_Gather(&comp_ratio, 1, MPI_DOUBLE, compression_ratios, 1, MPI_DOUBLE, 0, first_half_ranks);
                
            //Send information on amount of bytes to transfer and the buffer size
            MPI_Send(&compressed_bytes, 1, MPI_UNSIGNED_LONG_LONG, couple, 3, MPI_COMM_WORLD);
    
            //Execute compressed ping pong here
            execute_compressed_ping_pong(compressed_data, compressed_bytes, compressed_times,  m_size,backend_flag);
    
            decompressed_data_host = new double[m_size];
            ZFPCompressor<double> decompressor(decompressed_data_host, m_size, user_value,0);
            dec_start_time = MPI_Wtime();
            decompressor.decompress_fixed_accuracy(compressed_data, bufsize);
            dec_end_time = MPI_Wtime();
            dec_time = dec_end_time - dec_start_time;
            MPI_Gather(&dec_time, 1, MPI_DOUBLE, decompression_times, 1, MPI_DOUBLE, 0, first_half_ranks);
            double overhead = compression_times[i]+decompression_times[i];
            MPI_Gather(&overhead, 1, MPI_DOUBLE, zfp_overhead, 1, MPI_DOUBLE, 0, first_half_ranks);
            //writeArrayToFile(decompressed_data_host, m_size, 2);
            long double root_square_error = compute_l2_error(data, decompressed_data_host, m_size);
            MPI_Gather(&root_square_error, 1, MPI_LONG_DOUBLE, rmse, 1, MPI_LONG_DOUBLE, 0, first_half_ranks);
            delete[] decompressed_data_host;
        }
    }
        else{ //Second half of MPI ranks just receive number of compressed bytes and then execute ping-pong
            MPI_Recv(&compressed_bytes, 1, MPI_UNSIGNED_LONG_LONG, couple, 3, MPI_COMM_WORLD, &status);
            execute_compressed_ping_pong(compressed_data, compressed_bytes, compressed_times, m_size,backend_flag);
        }
}


int main(int argc, char* argv[]){
    int my_rank, comm_size, cases, m_size = 131072; //53760;
    cudaError_t mallocErr, kernelErr, syncErr;
    int numThreads = 1;
    int deviceID;
    int numberOfSMs;
    int num_devices;


    //ZFP related declarations
    double user_value; //This variable will hold either the desired accuracy or the desired rate
    double* original_data_host;
    double* original_data_device;

    //Reporting variables
    int* array_sizes = NULL; 
    double* uncompressed_times = NULL;
    double* compressed_times = NULL;
    double* compression_times = NULL;
    double* transferDtoH_times = NULL;
    double* transferHtoD_times = NULL; 
    double* decompression_times = NULL;
    double* zfp_overhead = NULL;
    double* compression_ratios = NULL;
    double* communication_speedups = NULL;
    double* actual_performance_gains = NULL;
    long double* rmse = NULL;

    bool flagFR = false;
    bool flagFA = false;
    int backend_flag = 0;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    
    //cases = log2(max_size);
    cases = 1;

    if(argc<3){
        std::cout << "Usage: ./multi_cpp_gpu -m <message size> -<fa/fr> <value> -t <numThreads> -b <0:CPU|1:GPU>\n" << std::endl;
        MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
        return 1;
    }

    if (argc > 1) {
        // Iterate over the command line arguments
        for (int i = 1; i < argc; i++) {
            // Check for "-fr" flag
            if (strcmp(argv[i], "-fr") == 0) {
                flagFR = true;
                user_value = std::atof(argv[i+1]);
                if(my_rank == 0){std::cout << "Running on fixed-rate mode" << std::endl;}
            }
            // Check for "-fa" flag
            else if (strcmp(argv[i], "-fa") == 0) {
                flagFA = true;
                user_value = std::atof(argv[i+1]);
                if(my_rank == 0){std::cout << "Running on fixed-accuracy mode" << std::endl;}
            }
            else if (strcmp(argv[i], "-t") == 0){
                numThreads = std::atoi(argv[i+1]);
                if(my_rank == 0){std::cout << "Compression will be executed with "<< numThreads << " threads"<< std::endl;}

            } 
            else if (strcmp(argv[i], "-b") == 0){
                backend_flag = std::atoi(argv[i+1]);
                if(my_rank == 0){std::cout << "Compression will be executed on backend "<< backend_flag << std::endl;}

            }
            else if(strcmp(argv[i], "-m") == 0){
                m_size = std::atoi(argv[i+1]);
                if(my_rank == 0){std::cout << "Using message size of: "<< m_size*sizeof(double) << std::endl;}
            }
        }
    }

    if(backend_flag == 1){
        //Verify that each rank gets one GPU    
        cudaGetDeviceCount(&num_devices);
        cudaSetDevice(my_rank%num_devices);
        cudaGetDevice(&deviceID);
        cudaDeviceGetAttribute(&numberOfSMs, cudaDevAttrMultiProcessorCount, deviceID);
        //std::cout << "Rank "<< my_rank <<"Device ID: " << deviceID << ", Number of SMs: " << numberOfSMs << std::endl;
    }

    if(my_rank == 0){ 
        array_sizes = new int[comm_size/2];
        uncompressed_times = new double[comm_size/2];
        compressed_times = new double[comm_size/2];
        compression_times = new double[comm_size/2];
        decompression_times = new double[comm_size/2];
        transferDtoH_times = new double[comm_size/2];
        transferHtoD_times = new double[comm_size/2];
        zfp_overhead = new double[comm_size/2];
        compression_ratios = new double[comm_size/2];
        communication_speedups = new double[comm_size/2];
        actual_performance_gains = new double[comm_size/2];
        rmse = new long double[comm_size/2];
    }

    int mode = 0; //Running with fixed-rate compression
    if(flagFA){
        mode = 1; //Running with fixed-accuracy compression
    }

    //Allocate space both on host and device for original data
    original_data_host = new double[m_size];
    if(flagFR){
        if (backend_flag == 1){
            if(my_rank == 0){std::cout << "Executing Fixed-Rate compression on GPU" << std::endl;}
            mallocErr = cudaMalloc(&original_data_device, m_size*sizeof(double));
            if(mallocErr != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(mallocErr));
            
            initialize_array_on_host(original_data_host, m_size);
            cudaMemcpy(original_data_device,original_data_host, m_size*sizeof(double), cudaMemcpyHostToDevice);
            if(my_rank == 0){std::cout << "Data initialized on host and transferred to device: " << std::endl;}
           
            //if(my_rank < comm_size/2){ 
            //    //Copy original data back to host to verify correct initialization
            //    double* probe_original_data = new double[m_size];
            //    cudaMemcpy(probe_original_data,original_data_device, m_size*sizeof(double), cudaMemcpyDeviceToHost);
            //    writeArrayToFile(probe_original_data, m_size, 0, my_rank);
            //    delete[] probe_original_data;
            //} 
            if(my_rank==0){
                std::cout << "Executing uncompressed ping-pong" << std::endl;
            }
            execute_uncompressed_ping_pong(original_data_device, m_size, array_sizes, uncompressed_times, backend_flag, transferDtoH_times, transferHtoD_times);
            
            if(my_rank == 0){
                std::cout << "Going into prepare_compressed_ping_pong" << std::endl;
            }
            MPI_Barrier(MPI_COMM_WORLD);
            prepare_compressed_ping_pong(original_data_device, m_size, mode, backend_flag, user_value, numThreads,
                                         compressed_times, compression_times, decompression_times,
                                         zfp_overhead, compression_ratios, rmse);

        }else{
            if(my_rank == 0){std::cout << "Executing Fixed-Rate compression on CPU" << std::endl;}
            initialize_array_on_host(original_data_host, m_size);
            if(my_rank == 0){
                //writeArrayToFile(original_data_host, m_size, 0);
                std::cout << "Data initialized on host" << std::endl;
            }
            execute_uncompressed_ping_pong(original_data_host, m_size, array_sizes, uncompressed_times, backend_flag);
            prepare_compressed_ping_pong(original_data_host, m_size, mode, backend_flag, user_value, numThreads,
                                         compressed_times, compression_times, decompression_times,
                                         zfp_overhead, compression_ratios,rmse);
        }
    }
    else if(flagFA){
        if(my_rank == 0){std::cout << "Executing Fixed-Accuracy compression on CPU" << std::endl;}
        initialize_array_on_host(original_data_host, m_size);
            if(my_rank == 0){
                //writeArrayToFile(original_data_host, m_size, 0);
                std::cout << "Data initialized on host" << std::endl;
            }
        execute_uncompressed_ping_pong(original_data_host, m_size, array_sizes, uncompressed_times, backend_flag);
        prepare_compressed_ping_pong(original_data_host, m_size, mode, backend_flag, user_value, numThreads,
                                         compressed_times, compression_times, decompression_times,
                                         zfp_overhead, compression_ratios,rmse);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    if (my_rank == 0 ){
        std::cout << "Computing communication speedups and performance gains" << std::endl;
        for(int i=0; i<comm_size/2;i++){
            communication_speedups[i] = uncompressed_times[i]/compressed_times[i];
            actual_performance_gains[i] = uncompressed_times[i]/(compressed_times[i]+zfp_overhead[i]);
        }

        std::cout << "Writing results to file" << std::endl;
        writeResultsToCSV(array_sizes,uncompressed_times, compressed_times, compression_times, decompression_times,
                         zfp_overhead, compression_ratios, communication_speedups, actual_performance_gains,numThreads, user_value, rmse);
        //writeResultsToCSV(array_sizes,uncompressed_times, compressed_times, compression_times, decompression_times,
        //                 zfp_overhead, compression_ratios, communication_speedups, actual_performance_gains,numThreads, 
        //                 user_value, rmse, transferDtoH_times, transferHtoD_times);
        std::cout << "Cleaning in progress" << std::endl;
        delete[] uncompressed_times;
        delete[] compressed_times;
        delete[] communication_speedups;
        delete[] actual_performance_gains;
        delete[] array_sizes;
        delete[] compression_times;
        uncompressed_times = nullptr;
        compressed_times = nullptr;
        communication_speedups = nullptr;
        actual_performance_gains = nullptr;
        array_sizes = nullptr;
        compression_times = nullptr;
    }   

    //Free memory allocations
    delete[] original_data_host;
    if(backend_flag == 1){
        cudaFree(original_data_device);
    }
    MPI_Finalize();
    return 0;
}
