cmake_minimum_required(VERSION 3.13)

# ---------------------------------------------------------------------------- #

# use the same compiler as used for building zfp
#set(CMAKE_C_COMPILER "icc")
#set(CMAKE_CXX_COMPILER "icx")
#set(CMAKE_Fortran_COMPILER "ifort")

set(CMAKE_C_COMPILER "gcc")
set(CMAKE_CXX_COMPILER "g++")
set(CMAKE_Fortran_COMPILER "gfortran")

IF(NOT ZFP_DIR)
    message("Please provide the path through ZFP (-DZFP_DIR)")
ENDIF()

# ---------------------------------------------------------------------------- #

project(zfp_sandbox)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

enable_language(CXX C Fortran)

# Locate zfp paths ----------------------------------------------------------- #

set(ZFP_LIBRARY_DIR ${ZFP_DIR}/lib)
IF(EXISTS ${ZFP_LIBRARY_DIR})
    set(ZFP_LIBRARY_DIR ${ZFP_DIR}/lib)
    message("Found zfp lib at ${ZFP_LIBRARY_DIR}")
ELSEIF(EXISTS ${ZFP_DIR}/lib64)
    set(ZFP_LIBRARY_DIR ${ZFP_DIR}/lib64)
    message("Found zfp lib64 at ${ZFP_LIBRARY_DIR}")
ELSE()
    message("Check ZFP_DIR: Couldn't find zfp library at ${ZFP_LIBRARY_DIR}")
    message(FATAL_ERROR
            "Check ZFP_DIR: Couldn't find zfp library at ${ZFP_LIBRARY_DIR}64")
ENDIF()

set(ZFP_INCLUDE_DIR ${ZFP_DIR}/include)
IF(EXISTS ${ZFP_INCLUDE_DIR})
    message("Found zfp include at ${ZFP_INCLUDE_DIR}")
ELSE()
    message(FATAL_ERROR
            "Check ZFP_DIR: Couldn't find zfp include at ${ZFP_INCLUDE_DIR}")
ENDIF()

set(ZFP_MODULE_DIR ${ZFP_DIR}/modules)
IF(EXISTS ${ZFP_MODULE_DIR})
    message("Found zfp module at ${ZFP_MODULE_DIR}")
ELSE()
    message(FATAL_ERROR
            "Check ZFP_DIR: Couldn't find zfp module at ${ZFP_MODULE_DIR}")
ENDIF()

# ---------------------------------------------------------------------------- #

add_subdirectory(mpi_communication)


# ---------------------------------------------------------------------------- #

unset(ZFP_DIR)

