# Compressed MPI benchmarks

This repository holds tests done with GENE output files to study and analyze different ZFP library compression modes on this data. 

## Required Modules

This is the list of modules loaded on Raven to compile and execute the code inside mpi_communication:

$ module load gcc/11 cuda/11.6 openmpi/4.1 openmpi_gpu/4.1

## Getting started

1. To be able to run this codes, you will ned a ZFP build with support for OpenMP and CUDA. 

## Building ZFP with OpenMP and CUDA support

```
cd zfp-1.0.0
mkdir build
cd build
cmake -DCMAKE_C_COMPILER=<path/to/c-compiler> -DCMAKE_CXX_COMPILER=<path/to/c++-compiler> -DCMAKE_FC_COMPILER=<path/to/fortran-compiler> -DBUILD_CFP=ON -DBUILD_ZFORP=ON -DBUILD_EXAMPLES=ON / 
                                    -DBUILD_TESTING=ON -DBUILD_SHARED_LIBS=ON -DZFP_WITH_OPENMP=ON -DZFP_WITH_CUDA=ON -DZFP_BIT_STREAM_WORD_SIZE=64 ..
make
```

2. Once a ZFP build is available, go into the compressed_ping_pong directory.

## Building MPI compressed benchmarks

You should start by creating a build directoy. Then enter the build directory and type the cmake command.
The ZFP_DIR variable should be passed to the cmake command. 

```
mkdir build
cd build
cmake .. -DZFP_DIR=/path/to/zfp
make
```
## Benchmarks and what they do

1. cpp_gpu_single_size.cpp : this is the most basic version of the multi-pair ping pong code. It uses blocking send/recv and you can see how this is run on the submit_baseline_cpu_gpu_single_size.slurm file. The -b flag specifies if
running on GPU (1) or CPU (0). (For CPU runs, a -t command line flag is passed to indicate how many threads to use for ZFP compression on the CPU. This should not affect MPI communications whatsoever)

2. breakdown_cpp_gpu.cpp and breakdown_cpp_gpu_per_rank.cpp implement a manual device-to-device transfer to compare against the CUDA-aware results from 1. The transfers are fully blocked, meaning cudaMemcpy and blocking Send/Receive
operations are used. 

3. multi_pair_overlapped_asynchronous_gpu.cpp uses 4 different CUDA streams to overlap data transfers from device-to-host and back and MPI Isend/IRecv transfers.


## Executing
After building and making the code, the executable files will be located inside the build/bin directory. Example slurm files for cpp_gpu_single_size.cpp and multi_pair_overlapped_asynchronous_gpu.cpp are provided. 
Further information on run configurations is provided inside said slurm files. 

## Results
If the code completes without issues, a csv file should be created with a description of results.
Depending on which benchmark you execute, more or less data columns are shown. 

1. For cpp_gpu_single_size.cpp a file named using the convention: results_<fixed_rate>_<omp_threads>_backend_<0:CPU|1:GPU)>.csv is created. This file contains the following columns:

| Array Size [B] | Unc. Time (s) | Com. Time (s) | Comp. Ratio | Comm. Speedup | Compression Time | Decompression Time | ZFP Overhead | Actual Perf. Gain | RMSE |
|----------------|---------------|---------------|-------------|---------------|------------------|--------------------|--------------|-------------------|------|

2. For breakdown_cpp_gpu.cpp and breakdown_cpp_gpu_per_rank: 

| Array Size [B] | Rank | Unc. Time (s) | Com. Time (s) | Comp. Ratio | Comm. Speedup | Compression Time | Decompression Time | ZFP Overhead | Actual Perf. Gain | RMSE | DtoH_time | HtoD_time |
|----------------|------|---------------|---------------|-------------|---------------|------------------|--------------------|--------------|-------------------|------|-----------|-----------|

3. For multi_pair_overlapped_asynchronous_gpu.cpp:

| Array Size [B] | Rank | Unc. Time (s) | Com. Time (s) | Comp. Ratio | Comm. Speedup | Compression Time | Decompression Time | ZFP Overhead | Actual Perf. Gain | RMSE |
|----------------|------|---------------|---------------|-------------|---------------|------------------|--------------------|--------------|-------------------|------|


### Meanings

- Rank: for some of the benchmarks, the results are reported per rank so that a statistical analysis of variance might be possible.
- Unc. Time (s): total time for a Send/Receive pair of [Array size [Bytes]] with the uncompressed data ("original data").
- Com. Time (s): total time for a Send/Receive pair with ZFP compressed data of original data (message size then is smaller than original and thus this should be ideally lower)
- Comp. Ratio: achieved compression ratio.
- Comm. Speedup: simple ratio of Unc. Time/Com. Time
- Compression/Decompression Time: time taken by ZFP compression/decompression operations.
- ZFP Overhead: sum of Compresssion + Decompression Time.
- Actual Perf. Gain: computed as the ratio of Unc. Time/(Com. Time + ZFP Overhead)
- RMSE: root mean square error of decompressed data with respect to original data. 
- DtoH_time: time taken by cudaMemcpy from Device to Host.
- HtoD_time: time taken by cudaMemcpy from Host to Device. 


### Computing Bandwidth


| Array Size [B] | Unc. Time (s) | Com. Time (s) | Comp. Ratio | Comm. Speedup | Compression Time | Decompression Time | ZFP Overhead | Actual Perf. Gain | RMSE        |
|----------------|---------------|---------------|-------------|---------------|------------------|--------------------|--------------|-------------------|-------------|
| 536870912      | 0.175373      | 0.0861044     | 2           | 2.03675       | 0.656983         | 6.02196            | 6.67894      | 0.0259234         | 8.72556E-13 |

So, to compute the bandwidth for the previous example the following should be taken into consideration:

- Unc. Time: is the time for the Send/Recv pair. To compute Bandwidth, you should take into account only half of this time (assuming Send time ~ Receive Time)
- Array size is expressed in Bytes
- Timing results reported in the csv files correspond to a single pair of processes executing the ping-pong. However, to compute the full node bandwidth you should multiply by the amount of processes in a node.

For example: 

In Raven, each node has 4 processes, so the following calculation is done with the example results:

$$ BW = { Array Size * 8 \over (Unc. Time/2)} * 4 $$ 
$$ BW = { 536870912 * 8 \over (0.175373/2)} * 4 $$
$$ BW = 195 Gbits/s $$
	
## Authors and acknowledgment
Diego Jimenez Vargas

Max Planck Computing and Data Facility
